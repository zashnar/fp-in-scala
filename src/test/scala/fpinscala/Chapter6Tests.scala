package fpinscala
import fpinscala.chapter6.RNG
import org.scalatest.{Matchers, FlatSpec}

/**
  * Created by Jeff on 12/9/2015.
  */
class Chapter6Tests extends FlatSpec with Matchers {

  "RNG.nonNegativeInt" should "generate non-negative integers" in {
    (Int.MinValue to Int.MinValue).foreach( seed ⇒ {
      val engine = RNG.Engine(seed)
      val (value, _) = RNG.nonNegativeInt(engine)

      withClue(s"seed $seed, value $value") {
        (value >= 0) shouldBe true
      }
    })
  }
}
