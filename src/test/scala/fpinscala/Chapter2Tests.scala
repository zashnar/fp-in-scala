package fpinscala
import fpinscala.Chapter2._
import org.scalatest.{FlatSpec, Matchers}

/**
 * Created by jefflongueil on 10/3/15.
 */
class Chapter2Tests extends FlatSpec with Matchers {
  import Chapter2._

  "fib" should "return correct values" in {
    val result = List(fib(0), fib(1), fib(2), fib(3), fib(4), fib(5), fib(6))
    result should be(List(0, 1, 1, 2, 3, 5, 8))
  }

  "isSorted" should "hold true for sorted array" in {

    val dataLowToHigh = Array(0, 1, 2, 4, 6, 8, 16, 27, 38)
    isSorted(dataLowToHigh, (a: Int, b: Int) ⇒ b > a) shouldBe true

    val characters = Array('a', 'd', 'f', 'm', 'q', 'z')
    isSorted(characters, (a: Char, b: Char) ⇒ b > a) shouldBe true

  }

  it should "be false for unsorted array" in {

    val dataLowToHigh = Array(0, 1, 2, 44, 6, 8, 16, 27, 38)
    isSorted(dataLowToHigh, (a: Int, b: Int) ⇒ b > a) shouldBe false

    val characters = Array('a', 'd', 'f', 'c', 'q', 'z')
    isSorted(characters, (a: Char, b: Char) ⇒ b > a) shouldBe false

  }

  "curry" should "curry the function" in {
    def label (name:String, age:Int) : String = s"$name = $age"
    val curried = curry(label)
    val result = curried("fred")(42)
    result shouldBe "fred = 42"
  }

  "uncurry" should "uncurry the function" in {
    def curried (a:String) : (Int ⇒ String) = (b:Int) ⇒ s"$a = $b"
    curried ("fred")(42) shouldBe "fred = 42"
    val uncurried = uncurry(curried)
    uncurried("fred", 42) shouldBe "fred = 42"
  }

  "compose" should "compose the 2 functions" in {
    def f1double (a:Int) : Double = 2.0 * a
    def f2string (b:Double) : String = s"val = $b"
    val composed = compose (f2string, f1double)
    val result = composed(42)
    result shouldBe "val = 84.0"
  }
}
