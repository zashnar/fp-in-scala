package fpinscala
import org.scalatest.{Matchers, FlatSpec}

/**
 * Created by jefflongueil on 10/10/15.
 */
class Chapter3Tests extends FlatSpec with Matchers {
  import Chapter3._
  import Chapter3.List._

  "exercise 1" should "have correct result" in {
    val result = exercise1

    result shouldBe 3
  }

  "exercise 2" should "use tail to remove the head of a non empty list " in {
    tail(List(1, 2, 3, 4)) should be(List(2, 3, 4))
  }

  it should "use tail to return Nil for tail of empty list" in {
    tail(Nil) should be(Nil)
  }

  "exercise 3" should "use setHead to replace head of non empty list" in {
    setHead(5, List(1, 2, 3, 4)) should be(List(5, 2, 3, 4))
  }

  it should "use setHead to set head of empty list" in {
    setHead(5, Nil) should be(List(5))
  }

  "drop" should "drop N elements from a non empty list" in {
    val x1 = List(1, 2, 3, 4, 5, 6, 7, 8, 9)
    drop(x1, 5) should be(List(6, 7, 8, 9))
    drop(x1, 0) should be(x1)
    drop(x1, 1) should be(tail(x1))
  }

  it should "drop no elements from an emtpy list" in {
    val x = Nil
    drop(x, 5) should be(Nil)
    drop(x, 1) should be(Nil)
    drop(x, 0) should be(x)
  }

  "exercise 5" should "use dropWhile correctly" in {
    val data1 = List(1, 3, 5, 6, 7, 8, 9)
    val data2 = List(2, 4, 6, 8, 10, 11, 12, 13, 14)
    dropWhile(data1, (x: Int) ⇒ x % 2 == 1) should be(List(6, 7, 8, 9))
    dropWhile(data2, (x: Int) ⇒ x % 2 == 0) should be(List(11, 12, 13, 14))
    dropWhile(Nil, (x: Int) ⇒ x % 2 == 0) should be(Nil)
  }

  "reverse" should "reverse the contents of a list" in {
    reverse(List(1, 3, 7, 8, 9)) should be(List(9, 8, 7, 3, 1))
    reverse(Nil) should be(Nil)
    reverse(List(10, 9)) should be(List(9, 10))
  }

  "exercise 6" should "use init to result all but last elem in" in {
    val list1 = List(1, 3, 5, 6, 12)
    init(list1) should be(List(1, 3, 5, 6))
    init(Nil) should be(Nil)
  }

  "exercise 7" should "use new product impl to try to short circuit on 0.0" in {
    product2(List(1, 2, 3)) should be(6)
    product2(List(2, 10, 100)) should be(2000)
    product2(Nil) should be(1)
    product2(List(2)) should be(2)
  }

  "std list" should "demonstrate correct product behaviour" in {
    import scala.collection.immutable.List
    List(1, 2, 3).product should be(6)
    List(2, 10, 100).product should be(2000)
    //List().product should be (0)
    List(2).product should be(2)
  }

  "exercise 8" should "be even more tedious than exercise 7" in {
    val result = foldRight(List(1, 2, 3), Nil: List[Int])(Cons(_, _))
    result should be(List(1, 2, 3))
  }

  "exercise 9" should "compute the len of a list with foldRight" in {
    val result = foldRight(List(1, 2, 3, 4, 5), 0)((_, b) ⇒ b + 1)
    result should be(5)
  }

  "exercise 10" should "do good things with foldLeft" in {
    val list = List(1, 2, 3, 4, 15)
    foldLeft(list, 0)((a, _) ⇒ a + 1) should be(5)
    foldLeft(list, 1)(_ * _) should be(360)
  }

  "exercise 11" should "use product3 to good effect" in {
    product3(List(1, 2, 3)) should be(6)
    product3(List(2, 10, 100)) should be(2000)
    product3(Nil) should be(1)
    product3(List(2)) should be(2)
  }

  it should "use sum3 correctly" in {
    sum3(List(0, 4, 2, 10)) should be(16)
    sum3(List(5)) should be(5)
    sum3(Nil) should be(0)
  }

  it should "use len correctly" in {
    len(List(1, 2, 3, 4, 15)) should be(5)
    len(Nil) should be(0)
    len(List("hi", "there")) should be(2)
    len(List(5.4, 3.71, -8.9)) should be(3)
  }

  "exercise 12" should "use foldLeft-based reverse2 correctly" in {
    reverse2(List(1, 3, 7, 8, 9)) should be(List(9, 8, 7, 3, 1))
    reverse2(Nil) should be(Nil)
    reverse2(List(10, 9)) should be(List(9, 10))
  }

  "exercise 13" should "use foldLeft in terms of foldRight" in {
    val result1 = foldRightByLeft(List(1, 2, 3), Nil: List[Int])(Cons(_, _))
    result1 should be(List(1, 2, 3))
    val result2 = foldRightByLeft(List(1, 2, 3, 4, 5), 0)((_, b) ⇒ b + 1)
    result2 should be(5)
  }

  it should "use foldRight in terms of foldLeft" in {
    val list = List(1, 2, 3, 4, 15)
    foldLeftByRight(list, 0)((a, _) ⇒ a + 1) should be(5)
    foldLeftByRight(list, 1)(_ * _) should be(360)
  }

  "exercise 14" should "use append to add elems to list" in {
    val list = List(1, 4, 21, 18, 44, 100, 6, 7)
    append(list, 42) should be(List(1, 4, 21, 18, 44, 100, 6, 7, 42))
    append(Nil, 0) should be(List(0))
  }

  "exercise 15" should "turn a list of lists into a list" in {
    val list1 = List(1, 4, 21, 18, 44, 100)
    val list2 = List(5, 3, 1)
    val list3 = List(-5, 77, 99, 101)
    val list4 = List(8, 2, -30, -67)
    val listA = List(list1, list2, list3, list4)

    flatten(listA) should be(List(1, 4, 21, 18, 44, 100, 5, 3, 1, -5, 77, 99, 101, 8, 2, -30, -67))
  }

  "exercise 16" should "use xformInts to add 1 to each elem of a list of ints" in {
    val list = List(4, 5, -1, 10)
    xformInts(list) should be(List(5, 6, 0, 11))
  }

  "exercise 17" should "transform list of doubles to list of strings" in {
    val list = List(4.0, 5.3, -1.5, 10.42)
    dblToString(list) should be(List("4.0", "5.3", "-1.5", "10.42"))
  }

  "exercise 18" should "use map to accomplish xformInts and dblToString" in {
    val list1 = List(4, 5, -1, 10)
    val list2 = List(4.0, 5.3, -1.5, 10.42)

    map(list1)(_ + 1) should be(List(5, 6, 0, 11))
    map(list2)(_.toString) should be(List("4.0", "5.3", "-1.5", "10.42"))
  }

  "exercise 19" should "use filter to remove elems" in {
    val list1 = List(1, 2, 3, 4, 5, 6)
    val list2 = List("apple", "berry", "cherry", "aardvark", "arrow")

    filter(list1)(x ⇒ x % 2 == 1) should be(List(1, 3, 5))
    filter(list2)(x ⇒ !x.startsWith("a")) should be(List("berry", "cherry"))
  }

  "exercise 20" should "do us some flatmap'n" in {
    val list1 = List(1, 2, 3)
    val list2 = List("Ground", "control", "to", "Major", "Tom")
    flatMap(list1)(i => List(i, i)) should be(List(1, 1, 2, 2, 3, 3))
    flatMap(list2)(s => List(s.toCharArray.toSeq: _*)) should be(
      List('G', 'r', 'o', 'u', 'n', 'd', 'c', 'o', 'n', 't', 'r', 'o', 'l',
           't', 'o', 'M', 'a', 'j', 'o', 'r', 'T', 'o', 'm'))
  }

  "exercise 21" should "do filter via flatmap" in {
    val list1 = List(1, 2, 3, 4, 5, 6)
    val list2 = List("apple", "berry", "cherry", "aardvark", "arrow")

    filterByFlatMap(list1)(x ⇒ x % 2 == 1) should be(List(1, 3, 5))
    filterByFlatMap(list2)(x ⇒ !x.startsWith("a")) should be(List("berry", "cherry"))
  }

  "exercise 22" should "add two integer lists together" in {
    addLists(List(1, 2, 3), List(4, 5, 6)) should be(List(5, 7, 9))
    addLists(List(-4, 7, 2), List(8, 12)) should be(List(4, 19, 2))
  }

  "exercise 23" should "use zipWith to add lists of disparate types" in {
    zipWith(List(1, 2, 3), List(4, 5, 6))(_ + _) should be(List(5, 7, 9))
    zipWith(List(-4, 7, 2), List(8, 12))(_ + _) should be(List(4, 19))
    zipWith(List(1, 2, 3), List("hi", "there", "person")) {
                                                            (a, b) ⇒ s"$a$b"
                                                          } should be(List("1hi", "2there", "3person"))
  }

  "exercise 24" should "use hasSubsequence correctly" in {
    val list = List(42, 2, 4, 5, 10, 20, 2, 4, 88)
    hasSubsequence(list, List(5, 10)) shouldBe true
    hasSubsequence(list, List(42)) shouldBe true
    hasSubsequence(list, List(4)) shouldBe true
    hasSubsequence(list, List(20)) shouldBe true
    hasSubsequence(list, List(2, 4, 5, 10, 20)) shouldBe true
    hasSubsequence(list, List(42, 2, 4, 5, 10)) shouldBe true
    hasSubsequence(list, List(42, 2, 99)) shouldBe false
    hasSubsequence(list, list) shouldBe true
    hasSubsequence(list, Nil) shouldBe true
    hasSubsequence(list, List(2, 4, 88)) shouldBe true
  }

  "exercise 25" should "use size func to get correct size of trees" in {
    val t1 = Branch(Leaf(42), Leaf(21))
    val t2 = Branch(Leaf(99), t1)
    val t3 = Branch(Branch(Leaf(77), t2), t1)
    val t4 = Branch(t2, t3)
    val t5 = Leaf(5)

    Tree.size(t1) shouldBe 3
    Tree.size(t2) shouldBe 5
    Tree.size(t3) shouldBe 11
    Tree.size(t4) shouldBe 17
    Tree.size(t5) shouldBe 1
  }

  "exercise 26" should "use maximum to correctly calculate max value of trees of int" in {
    val t1 = Branch(Leaf(42), Leaf(21))
    val t2 = Branch(Leaf(99), t1)
    val t3 = Branch(Branch(Leaf(77), t2), t1)
    val t4 = Branch(t2, t3)
    val t5 = Leaf(5)

    Tree.maximum(t1) shouldBe 42
    Tree.maximum(t2) shouldBe 99
    Tree.maximum(t3) shouldBe 99
    Tree.maximum(t4) shouldBe 99
    Tree.maximum(t5) shouldBe 5
  }

  "exercise 27" should "use depth to correctly calculate depth value of trees" in {
    val t1 = Branch(Leaf(42), Leaf(21))
    val t2 = Branch(Leaf(99), t1)
    val t3 = Branch(Branch(Leaf(77), t2), t1)
    val t4 = Branch(t2, t3)
    val t5 = Leaf(5)

    Tree.depth(t1) shouldBe 2
    Tree.depth(t2) shouldBe 3
    Tree.depth(t3) shouldBe 5
    Tree.depth(t4) shouldBe 6
    Tree.depth(t5) shouldBe 1
  }

  "exercise 28" should "use map to correctly transform trees" in {
    val t1 = Branch(Leaf(42), Leaf(21))
    val t2 = Branch(Leaf(99), t1)

    Tree.map[Int, Int](t1, x ⇒ x + 10) should be(Branch(Leaf(52), Leaf(31)))
    Tree.map[Int, String](t2, x ⇒ s"*$x*") should be(Branch(Leaf("*99*"), Branch(Leaf("*42*"), Leaf("*21*"))))
  }

  "exercise 29" should "implement fold function correctly" in {
    val t1 = Branch(Leaf(42), Leaf(21))
    val t2 = Branch(Leaf(99), t1)
    val t3 = Branch(Branch(Leaf(77), t2), t1)

    import TreeWithFold._

    // add each (value + 10)
    fold(t1)(x ⇒ 10 + x)(_ + _) shouldBe 83

    // min value for tree
    fold(t2)(x ⇒ x)(math.min) shouldBe 21

    // add up values, converting to double adding 2.5 to each
    // math.min(aa.getOrElse(bb), bb)) shouldBe 21
    fold(t3)(x => x + 2.5)(_ + _) shouldBe 317.0 // 302 + (6 leafs * 2.5) = 302+15 = 317
  }

  it should "use fold-based size func to get correct size of trees" in {
    val t1 = Branch(Leaf(42.5), Leaf(21.8))
    val t2 = Branch(Leaf("hello"), Branch(Leaf("there"), Branch(Leaf("world"), Leaf("!"))))
    val t3 = Branch(Branch(Leaf(77), t2), t1)
    val t4 = Branch(t2, t3)
    val t5 = Leaf(new Exception("I'm an object too!"))

    TreeWithFold.size(t1) shouldBe 3
    TreeWithFold.size(t2) shouldBe 7
    TreeWithFold.size(t3) shouldBe 13
    TreeWithFold.size(t4) shouldBe 21 // t2 + t2 = 7 + 13 = 20 + 1 for new root = 21
    TreeWithFold.size(t5) shouldBe 1
  }

  it should "use fold-based maximum to correctly calculate max value of trees of int" in {
    val t1 = Branch(Leaf(42), Leaf(21))
    val t2 = Branch(Leaf(99), t1)
    val t3 = Branch(Branch(Leaf(77), t2), t1)
    val t4 = Branch(t2, t3)
    val t5 = Leaf(5)

    TreeWithFold.maximum(t1) shouldBe 42
    TreeWithFold.maximum(t2) shouldBe 99
    TreeWithFold.maximum(t3) shouldBe 99
    TreeWithFold.maximum(t4) shouldBe 99
    TreeWithFold.maximum(t5) shouldBe 5
  }

  it should "use fold-based depth to correctly calculate depth value of trees" in {
    val t1 = Branch(Leaf(42), Leaf(21))
    val t2 = Branch(Leaf(99), t1)
    val t3 = Branch(Branch(Leaf(77), t2), t1)
    val t4 = Branch(t2, t3)
    val t5 = Leaf(5)

    TreeWithFold.depth(t1) shouldBe 2
    TreeWithFold.depth(t2) shouldBe 3
    TreeWithFold.depth(t3) shouldBe 5
    TreeWithFold.depth(t4) shouldBe 6
    TreeWithFold.depth(t5) shouldBe 1
  }

  it should "use fold-based map to correctly transform trees" in {
    val t1 = Branch(Leaf(42), Leaf(21))
    val t2 = Branch(Leaf(99), t1)

    TreeWithFold.map[Int, Int](t1, x ⇒ x + 10) should be(Branch(Leaf(52), Leaf(31)))
    TreeWithFold.map[Int, String](t2, x ⇒ s"*$x*") should be(Branch(Leaf("*99*"), Branch(Leaf("*42*"), Leaf("*21*"))))
  }
}
