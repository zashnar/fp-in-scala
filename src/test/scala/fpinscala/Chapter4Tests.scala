package fpinscala
import java.io.IOException

import org.scalatest.{FlatSpec, Matchers}
import Chapter4._

/**
 * Created by jefflongueil on 10/19/15.
 */
class Chapter4Tests extends FlatSpec with Matchers {

  val someA = Some(42)
  val someB = Some(98.6)
  val someC = Some("hello")

  val noneA: Option[Int]    = None
  val noneB: Option[Double] = None
  val noneC: Option[String] = None

  //  def map[B](f: A => B): Option[B]
  "4.1 - Option.map" should "handle Some correctly" in {
    someA.map(_ * 2) shouldBe Some(84)
    someB.map(x ⇒ s"percent $x") shouldBe Some("percent 98.6")
    someC.map(x ⇒ s"$x world") shouldBe Some("hello world")
  }

  it should "handle None correctly" in {
    noneA.map(_ * 2) shouldBe None
    noneB.map(x ⇒ s"percent $x") shouldBe None
    noneC.map(x ⇒ s"$x world") shouldBe None
  }

  //  def flatMap[B](f: A => Option[B]): Option[B]
  "4.1 - Option.flatMap" should "handle Some correctly" in {
    someA.flatMap(x ⇒ Some(x % 2 == 0)) shouldBe Some(true)
    someA.flatMap(x ⇒ if (x % 2 == 1) Some(true) else None) shouldBe None
    someB.flatMap(x ⇒ Some(s"percent $x")) shouldBe Some("percent 98.6")
    someC.flatMap(x ⇒ Some(s"$x world")) shouldBe Some("hello world")
  }

  it should "handle None correctly" in {
    noneA.flatMap(x ⇒ Some(x % 2 == 0)) shouldBe None
    noneA.flatMap(x ⇒ if (x % 2 == 1) Some(true) else None) shouldBe None
    noneB.flatMap(x ⇒ Some(s"percent $x")) shouldBe None
    noneC.flatMap(x ⇒ Some(s"$x world")) shouldBe None
  }

  // def getOrElse[B >: A](default: => B): B
  "4.1 - Option.getOrElse" should "handle Some correctly" in {
    someA.getOrElse(99) shouldBe 42
    someB.getOrElse(123.456) shouldBe 98.6
    someC.getOrElse("world") shouldBe "hello"
  }

  it should "handle None correctly" in {
    noneA.getOrElse(99) shouldBe 99
    noneB.getOrElse(123.456) shouldBe 123.456
    noneC.getOrElse("world") shouldBe "world"
  }

  // def orElse[B >: A](ob: => Option[B]): Option[B]
  "4.1 - Option.orElse" should "handle Some correctly" in {
    someA.orElse(Some(99)) shouldBe Some(42)
    someB.orElse(Some(123.456)) shouldBe Some(98.6)
    someC.orElse(Some("world")) shouldBe Some("hello")
  }

  it should "handle None correctly" in {
    noneA.orElse(Some(99)) shouldBe Some(99)
    noneB.orElse(Some(123.456)) shouldBe Some(123.456)
    noneC.orElse(Some("world")) shouldBe Some("world")
  }

  // def filter(f: A => Boolean): Option[A]
  "4.1 - Option.filter" should "handle Some correctly" in {
    someA.filter(_ % 2 == 0) shouldBe Some(42)
    someA.filter(_ % 2 == 1) shouldBe None
    someB.filter(_ < 100.5) shouldBe Some(98.6)
    someB.filter(_ > 100.5) shouldBe None
    someC.filter(_.startsWith("he")) shouldBe Some("hello")
    someC.filter(_.startsWith("hx")) shouldBe None
  }

  it should "handle None correctly" in {
    noneA.filter(_ % 2 == 0) shouldBe None
    noneA.filter(_ % 2 == 1) shouldBe None
    noneB.filter(_ < 100.5) shouldBe None
    noneB.filter(_ > 100.5) shouldBe None
    noneC.filter(_.startsWith("he")) shouldBe None
    noneC.filter(_.startsWith("hx")) shouldBe None
  }

  "4.2 - variance" should "calculate variance correctly" in {
    val seq = List(2.0, 5.5, 6.9, 24.2, 3.8, 142.7)
    val mean = seq.sum / 6.0
    val diffs = seq.map(_ - mean)
    val squares = diffs.map(math.pow(_, 2))
    val expectedVariance = squares.sum / 6.0
    variance(seq) shouldBe Some(expectedVariance)
  }

  "4.3 - lifting" should "happen with map2" in {
    def add(a: Int, b: Int): Int = a + b
    def label(a: Int, b: String): String = s"$b $a"
    val x1 = map2(Some(7), None)(add)
    val x2 = map2(Some(5), Some(11))(add)
    val x3 = map2[Int, Int, Int](None, Some(11))(add)
    val x4 = map2(Some(9), Some("Number:"))(label)
    x1 shouldBe None
    x2 shouldBe Some(16)
    x3 shouldBe None
    x4 shouldBe Some("Number: 9")
  }

  "4.4 - sequence" should "evaluate collections of option values correctly" in {
    val x1 = List(Some(1), Some(4), Some(10), Some(8))
    val x2 = List(Some(1), Some(4), None, Some(8))
    val x3 = List("hello", "world", "what", "is", "up").map(x ⇒ Some(x))
    val x4 = List(Some("hello"), Some("world"), Some("what"), Some("is"), None)
    val x5: List[Option[String]] = Nil
    sequence(x1) should be(Some(List(1, 4, 10, 8)))
    sequence(x2) should be(None)
    sequence(x3) should be(Some(List("hello", "world", "what", "is", "up")))
    sequence(x4) should be(None)
    sequence(x5) should be(Some(Nil))
  }

  "4.5 - traverse" should "traverse collections" in {
    def f1(x: Int) = if (x % 2 == 0) Some(x) else None
    def f2(x: String) = if (x.startsWith("he")) Some(x) else None
    def f3(x: String) = if (x.length > 1) Some(x.substring(0, 2)) else None
    val xs1 = List(4, 5, 7, 13, 20, 24, 9, 12)
    val xs2 = List(1, 3, 9, 11)
    val xs3 = List("hello", "world", "send", "heavenly", "help", "for the", "heck", "of", "it")
    val xs4 = List("that", "was", "fun")
    traverse(xs1)(f1) should be(None)
    traverse(xs2)(f1) should be(None)
    traverse(xs3)(f2) should be(None)
    traverse(xs3)(f3) should be(Some(List("he", "wo", "se", "he", "he", "fo", "he", "of", "it")))
    traverse(xs4)(f2) should be(None)
    traverse(Nil)(f1) should be(Some(Nil))
    traverse(Nil)(f2) should be(Some(Nil))
  }

  it should "power sequence method to evaluate collections of option values correctly" in {
    val x1 = List(Some(1), Some(4), Some(10), Some(8))
    val x2 = List(Some(1), Some(4), None, Some(8))
    val x3 = List("hello", "world", "what", "is", "up").map(x ⇒ Some(x))
    val x4 = List(Some("hello"), Some("world"), Some("what"), Some("is"), None)
    val x5: List[Option[String]] = Nil
    sequence2(x1) should be(Some(List(1, 4, 10, 8)))
    sequence2(x2) should be(None)
    sequence2(x3) should be(Some(List("hello", "world", "what", "is", "up")))
    sequence2(x4) should be(None)
    sequence2(x5) should be(Some(Nil))
  }

  "4.6" should "have correct impls for map map2 flatMap and getOrElse" in {
    val hello: Either[String, Int] = Left("hello")
    val fortyTwo: Either[String, Int] = Right(42)

    // right
    fortyTwo.map(x ⇒ x * 2) should be(Right(84))
    fortyTwo.map2(Right(23))((a,b) ⇒ a + b) should be(Right(65))
    fortyTwo.orElse(Right(99)) should be(fortyTwo)
    fortyTwo.flatMap(x ⇒ Right(x * 3) ) should be(Right(126))

    // left
    hello.map(x ⇒ x * 2) should be(hello)
    hello.map2(hello)((a,b) ⇒ s"$a:$b") should be(hello)
    hello.orElse(Right(99)) should be(Right(99))
    hello.flatMap(x ⇒ Right(x * 3) ) should be(hello)

  }

  "4.7" should "use traverse correctly" in {
    import Either._

    val xs1 : List[Either[String,Int]] = List(Right(42), Right(21), Right(99))
    val xs2  : List[Either[String,Int]] = List(Right(21), Left("Fail!"), Right(88), Left("Boom!"))

    traverse(xs1)(_.map(r ⇒ r + 1)) should be(Right(List(43, 22, 100)))
    traverse(xs2)(_.map(r ⇒ r + 1)) should be(Left("Fail!"))
  }

  it should "use sequence correctly" in {
    import Either._

    val xs1 : List[Either[String,Int]] = List(Right(42), Right(21), Right(99))
    val xs2  : List[Either[String,Int]] = List(Right(21), Left("Fail!"), Right(88), Left("Boom!"))

    sequence(xs1) should be(Right(List(42, 21, 99)))
    sequence(xs2) should be(Left("Fail!"))
  }
}
