package fpinscala
import fpinscala.chapter5.{Empty, Stream}
import org.scalatest.{FlatSpec, Matchers}

/**
 * Created by jefflongueil on 11/15/15.
 */
class Chapter5Tests extends FlatSpec with Matchers {

  "5.1" should "convert a stream to a list" in {
    val s = Stream(5, 3, 9, 33, 19, 71)
    s.toList should be(List(5, 3, 9, 33, 19, 71))
  }

  "5.2" should "take 1st N items from stream" in {
    val s = Stream(5, 3, 9, 33, 19, 71)
    s.take(3).toList should be(List(5, 3, 9))
  }

  it should "drop 1st N items from stream" in {
    val s = Stream(5, 3, 9, 33, 19, 71)
    s.drop(3).toList should be(List(33, 19, 71))
  }

  "5.3" should "take elems while predicate holds" in {
    val s = Stream(5, 3, 9, 33, 99, 71)
    s.takeWhile(_ < 35).toList should be(List(5, 3, 9, 33))
  }

  "5.4" should "return true if predicate holds for all elements of stream" in {
    val s1 = Stream(5, 3, 9, 33, 99, 71)
    val s2 = Stream.empty[Int]
    s1.forAll(_ < 35) shouldBe false
    s1.forAll(_ < 100) shouldBe true
    s1.forAll(_ % 2 == 1) shouldBe true
    s2.forAll(_ < 35) shouldBe false
    s2.forAll(_ < 100) shouldBe false
    s2.forAll(_ % 2 == 1) shouldBe false
  }

  "5.5" should "take elems while predicate holds - using foldRight" in {
    val s1 = Stream(5, 3, 9, 33, 99, 71)
    val s2 = Stream.empty[Int]
    s1.takeWhile(_ < 35).toList should be(List(5, 3, 9, 33))
    s2.takeWhile(_ < 35).toList should be(Nil)
  }

  "5.6" should "use foldRight to implement headOption" in {
    val s1 = Stream(5, 3, 9, 33, 99, 71)
    val s2 = Stream.empty[Int]
    s1.headOption2 should be(Some(5))
    s2.headOption2 should be(None)
  }

  "5.7" should "map stream using function" in {
    val s1 = Stream("hello", "world", "how", "is", "it", "going?")
    val s2 = Stream.empty[String]

    s1.map(w ⇒ w.length).toList should be(List(5, 5, 3, 2, 2, 6))
    s2.map(w ⇒ w.length).toList should be(Nil)
  }

  it should "filter stream using function" in {
    val s1 = Stream("hello", "world", "how", "is", "it", "going?")
    val s2 = Stream.empty[String]

    s1.filter(w ⇒ w.length < 3).toList should be(List("is", "it"))
    s2.filter(w ⇒ w.length < 3).toList should be(Nil)
  }

  it should "flatmap stream using function" in {
    val s1 = Stream("hello", "there", "world")
    val s2 = Stream.empty[String]

    val s1x = List('h', 'e', 'l', 'l', 'o', 't, 'h', 'e', 'r', 'e', 'w', 'o', 'r', 'l', 'd')
    s1.flatMap(x ⇒ Stream(x.toCharArray)).toList should be (s1x)
    s2.flatMap(x ⇒ Stream(x.toCharArray)).toList should be (Nil)
  }

  it should "append 2 streams" in {
    val s1 = Stream(5, 3, 9, 33, 99, 71)
    val s2 = Stream(9, 8, 7, 1)
    val s3 = Stream.empty[Int]

    s1.append(s2).toList should be(List(5, 3, 9, 33, 99, 71, 9, 8, 7, 1))
    s2.append(s1).toList should be(List(9, 8, 7, 1, 5, 3, 9, 33, 99, 71))
    s1.append(s3).toList should be(s1.toList)
    s2.append(s3).toList should be(s3.toList)
    s3.append(s1).toList should be(s1.toList)
    s3.append(s2).toList should be(s2.toList)
  }
}
