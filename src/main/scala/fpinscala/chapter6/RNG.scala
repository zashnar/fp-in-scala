package fpinscala.chapter6

/**
  * Created by Jeff on 12/9/2015.
  */
trait RNG {
  def nextInt: (Int, RNG)
}

object RNG {
  case class Engine(seed: Long) extends RNG {
    def nextInt: (Int, RNG) = {
      val newSeed = (seed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL
      val nextRNG = Engine(newSeed)
      val n = (newSeed >>> 16).toInt
      (n, nextRNG)
    }
  }

  /**
    * Generates a random integer between 0 and Int.maxValue (inclusive)
    * @param rng Previous state
    * @return The random number and the next state
    */
  def nonNegativeInt(rng: RNG): (Int, RNG) = {
    val (nextInt, nextRng) = rng.nextInt
    val adjustedInt = nextInt match {
      case Int.MinValue ⇒ Int.MaxValue
      case -1           ⇒ 0
      case other        ⇒ -other
    }
    (adjustedInt, nextRng)
  }
}