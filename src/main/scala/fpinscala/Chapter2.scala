package fpinscala
import scala.annotation.tailrec

/**
 * Created by jefflongueil on 10/3/15.
 */
object Chapter2 {

  // Exercise 1: Write a function to compute the nth fibonacci number
  def fib(n: Int): Int = {

    def go(y: Int): Int = y match {
      case 0 ⇒ 0
      case 1 ⇒ 1
      case _ ⇒ fib(y - 1) + fib(y - 2)
    }

    go(math.max(n, 0))
  }

  // Exercise 2
  def isSorted[A](as: Array[A], ordered: (A, A) => Boolean): Boolean = {
    val last = as.length - 1
    @tailrec
    def loop(index: Int): Boolean = {
      if ((last - index) < 2) true
      else {
        val x = as(index)
        val y = as(index + 1)
        val ok = ordered(x, y)
        if (!ok) false
        else loop(index + 1)
      }
    }
    loop(0)
  }

  // Exercise 3
  def curry[A, B, C](f: (A, B) => C): A => (B => C) = {
    (a: A) ⇒ (b: B) ⇒ f(a, b)
  }

  // Exercise 4
  def uncurry[A, B, C](f: A => B => C): (A, B) => C = {
    (a: A, b: B) ⇒ f(a)(b)
  }

  // Exercise 5
  def compose[A,B,C](f: B => C, g: A => B): A => C = {
    (a:A) ⇒ f(g(a))
  }

}
