package fpinscala

import scala.{Option => _, Either => _, _}


/**
 * Created by jefflongueil on 10/19/15.
 */
object Chapter4 {

  sealed trait Option[+A] {
    def map[B](f: A => B): Option[B]
    def flatMap[B](f: A => Option[B]): Option[B]
    def getOrElse[B >: A](default: => B): B
    def orElse[B >: A](ob: => Option[B]): Option[B]
    def filter(f: A => Boolean): Option[A]
  }

  case class Some[+A](get: A) extends Option[A] {
    def map[B](f: A => B): Option[B] = Some(f(get))
    def getOrElse[B >: A](default: => B): B = get
    def flatMap[B](f: (A) ⇒ Option[B]): Option[B] = f(get)
    def filter(f: (A) ⇒ Boolean): Option[A] = if (f(get)) Some(get) else None
    def orElse[B >: A](ob: => Option[B]): Option[B] = Some(get)
  }

  case object None extends Option[Nothing] {
    def map[B](f: Nothing => B): Option[B] = None
    def getOrElse[B >: Nothing](default: => B): B = default
    def flatMap[B](f: (Nothing) ⇒ Option[B]): Option[B] = None
    def filter(f: Nothing => Boolean): Option[Nothing] = None
    def orElse[B >: Nothing](ob: => Option[B]): Option[B] = ob
  }

  sealed trait Either[+E, +A] {
    def map[B](f: A => B): Either[E, B]
    def flatMap[EE >: E, B](f: A => Either[EE, B]): Either[EE, B]
    def orElse[EE >: E, B >: A](b: => Either[EE, B]): Either[EE, B]
    def map2[EE >: E, B, C](b: Either[EE, B])(f: (A, B) => C): Either[EE, C]
  }

  case class Left[+E](value: E) extends Either[E, Nothing] {
    override def map[B](f: (Nothing) ⇒ B): Either[E, B] = this
    override def map2[EE >: E, B, C](b: Either[EE, B])(f: (Nothing, B) ⇒ C): Either[EE, C] = this
    override def orElse[EE >: E, B >: Nothing](b: ⇒ Either[EE, B]): Either[EE, B] = b
    override def flatMap[EE >: E, B](f: (Nothing) ⇒ Either[EE, B]): Either[EE, B] = this
  }

  case class Right[+A](value: A) extends Either[Nothing, A] {
    override def map[B](f: (A) ⇒ B): Either[Nothing, B] = Right(f(value))
    override def map2[EE >: Nothing, B, C](b: Either[EE, B])(f: (A, B) ⇒ C): Either[EE, C] = b.map(f(value, _))
    override def orElse[EE >: Nothing, B >: A](b: ⇒ Either[EE, B]): Either[EE, B] = this
    override def flatMap[EE >: Nothing, B](f: (A) ⇒ Either[EE, B]): Either[EE, B] = f(value)
  }

  object Either {
    def sequence[E, A](es: List[Either[E, A]]): Either[E, List[A]] = {
      traverse(es)(x => x)
    }

    def traverse[E, A, B](as: List[A])(f: A => Either[E, B]): Either[E, List[B]] = as match {
      case head :: tail ⇒ f(head).map2(traverse(tail)(f))(_ :: _)
      case Nil          ⇒ Right(Nil)
    }
  }

  // Option is because the sequence could be empty
  def variance(xs: Seq[Double]): Option[Double] = {
    // without using flatmap
    //    if (xs.nonEmpty) {
    //      val count = xs.length
    //      val mean = xs.sum / count
    //      val squares = xs.map(x ⇒ math.pow(x - mean, 2))
    //      val sqMean = squares.sum / count
    //      Some(sqMean)
    //    }
    //    else {
    //      None
    //    }
    // using flatmap... because exercise. // after looking at (contrived, but useful) answer
    mean(xs).flatMap(m ⇒ mean(xs.map(x ⇒ math.pow(x - m, 2))))
  }

  def mean(xs: Seq[Double]): Option[Double] = {
    if (xs.nonEmpty) Some(xs.sum / xs.length)
    else None
  }

  def map2[A, B, C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] = {
    a.flatMap(aval ⇒ b.map(bval ⇒ f(aval, bval)))
  }

  def sequence[A](a: List[Option[A]]): Option[List[A]] = a match {
    case Nil          ⇒ Some(Nil)
    case head :: tail ⇒ head flatMap (h ⇒ sequence(tail).map(h :: _))
  }

  def traverse[A, B](a: List[A])(f: A => Option[B]): Option[List[B]] = a match {
    case head :: tail ⇒ map2(f(head), traverse(tail)(f))(_ :: _)
    case Nil          ⇒ Some(Nil)
  }

  def sequence2[A](a: List[Option[A]]): Option[List[A]] = {
    traverse(a)(x ⇒ x)
  }
}


case class Person(name: Name, age: Age)
sealed class Name(val value: String)
sealed class Age(val value: Int)

object Person {
//
//  def mkName(name: String): Either[String, Name] =
//    if (name == "" || name == null) Left("Name is empty.")
//    else Right(new Name(name))
//
//  def mkAge(age: Int): Either[String, Age] =
//    if (age < 0) Left("Age is out of range.")
//    else Right(new Age(age))
//
//  def mkPerson(name: String, age: Int): Either[String, Person] =
//    mkName(name).map2(mkAge(age))(Person(_, _))


  import Chapter4.Either
  import Chapter4.Right
  import Chapter4.Left
  import Chapter4.Either._

  case class Fail (errors:List[String])

  def mkName(name: String): Either[Fail, Name] =
    if (name == "" || name == null) Left(Fail(List("Name is empty.")))
    else Right(new Name(name))

  def mkAge(age: Int): Either[Fail, Age] =
    if (age < 0) Left(Fail(List("Age is out of range.")))
    else Right(new Age(age))

  def mkPerson(name: String, age: Int): Either[Fail, Person] = {
    mkName(name).map2(mkAge(age))(Person(_, _))
  }

}