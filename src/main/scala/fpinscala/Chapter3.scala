package fpinscala
import scala.annotation.tailrec

/**
 * Created by jefflongueil on 10/10/15.
 */
object Chapter3 {

  sealed trait List[+T]
  case object Nil extends List[Nothing]
  case class Cons[+A](head: A, tail: List[A]) extends List[A]

  sealed trait Tree[+A]
  case class Leaf[A](value: A) extends Tree[A]
  case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

  object List {
    def sum(ints: List[Int]): Int = ints match {
      case Nil         ⇒ 0
      case Cons(x, xs) ⇒ x + sum(xs)
    }

    def product(ds: List[Double]): Double = ds match {
      case Nil            ⇒ 0.0
      case Cons(0.0, Nil) ⇒ 0.0
      case Cons(x, Nil)   ⇒ x
      case Cons(x, xs)    ⇒ x * product(xs)
    }

    def apply[A](as: A*): List[A] =
      if (as.isEmpty) Nil
      else Cons(as.head, apply(as.tail: _*))

    def tail[A](ls: List[A]): List[A] = ls match {
      case Nil         ⇒ Nil
      case Cons(x, xs) ⇒ xs
    }

    def setHead[A](elem: A, ls: List[A]): List[A] = Cons(elem, tail(ls))


    def drop[A](l: List[A], n: Int): List[A] = l match {
      case Nil                   ⇒ Nil
      case _ if n == 0           ⇒ l
      case Cons(x, xs) if n == 1 ⇒ xs
      case Cons(x, xs)           ⇒ drop(xs, n - 1)
    }

    def dropWhile[A](l: List[A], f: A => Boolean): List[A] = l match {
      case Nil                            ⇒ Nil
      case m@Cons(head, tail) if !f(head) ⇒ m
      case Cons(head, tail)               ⇒ dropWhile(tail, f)
    }

    def reverse[A](l: List[A]): List[A] = {
      @tailrec
      def loop(acc: List[A], rest: List[A]): List[A] = rest match {
        case Nil              ⇒ acc
        case Cons(head, tail) ⇒ loop(Cons(head, acc), tail)
      }
      loop(Nil, l)
    }

    def init[A](l: List[A]): List[A] = reverse(l) match {
      case Nil              ⇒ Nil
      case Cons(head, tail) ⇒ reverse(tail)
    }

    def foldRight[A, B](as: List[A], z: B)(f: (A, B) => B): B = as match {
      case Nil         => z
      case Cons(x, xs) => f(x, foldRight(xs, z)(f))
    }

    def foldRightByLeft[A, B](as: List[A], z: B)(f: (A, B) => B): B =
      foldLeft(reverse(as), z)((a, b) ⇒ f(b, a))

    def foldLeftByRight[A, B](as: List[A], z: B)(f: (B, A) => B): B =
      foldRight(reverse(as), z)((a, b) ⇒ f(b, a))

    def product2(ds: List[Double]): Double = foldRight(ds, 1.0)(_ * _)

    @tailrec
    def foldLeft[A, B](as: List[A], z: B)(f: (B, A) => B): B = as match {
        case Nil         ⇒ z
        case Cons(x, xs) ⇒ foldLeft(xs, f(z, x))(f)
    }

    // exercise 11
    def sum3(ints: List[Int]): Int = foldLeft(ints, 0)(_ + _)

    def product3(ds: List[Double]): Double = foldLeft(ds, 1.0)(_ * _)

    def len[A](l: List[A]): Int = foldLeft(l, 0)((a, _) ⇒ a + 1)

    // exercise 12
    def reverse2[A](list: List[A]): List[A] = {
      foldLeft(list, Nil: List[A])((xx, yy) ⇒ Cons(yy, xx))
    }

    // ex 14
    def append[A](ls: List[A], elem: A): List[A] = ls match {
      case Nil           ⇒ Cons(elem, Nil)
      case y@Cons(x, xs) ⇒ foldRight(y, List(elem))((a, acc) ⇒ Cons(a, acc))
    }

    // ex 15
    def flatten[A](ls: List[List[A]]): List[A] = {
      // start with last list, i.e. fold right
      // for each list
      // make a new list from the elem and the list so far
      foldRight(ls, Nil: List[A])((list, acc) ⇒ {
        foldRight(list, acc)((elem, theList) ⇒ {
          Cons(elem, theList)
        })
      })
    }

    // ex 16
    def xformInts(ls: List[Int]): List[Int] =
      foldRight(ls, Nil: List[Int])((elem, acc) ⇒ Cons(elem + 1, acc))

    // ex 17
    def dblToString(ls: List[Double]): List[String] =
      foldRight(ls, Nil: List[String])((elem, acc) ⇒ Cons(elem.toString, acc))

    // ex 18
    def map[A, B](as: List[A])(f: A => B): List[B] =
      reverse(foldLeft(as, Nil: List[B])((acc, elem) ⇒ Cons(f(elem), acc)))

    // ex 19
    def filter[A](as: List[A])(f: A => Boolean): List[A] =
      reverse(foldLeft(as, Nil: List[A])((acc, elem) ⇒ {
        if (f(elem)) Cons(elem, acc) else acc
      }))

    // ex 20
    def flatMap[A, B](as: List[A])(f: A => List[B]): List[B] = {
      foldRight(as, Nil: List[B])((elemA, acc) ⇒ {
        foldRight(f(elemA), acc)((elemB, theList) ⇒ {
          Cons(elemB, theList)
        })
      })
    }

    // ex 21
    def filterByFlatMap[A](as: List[A])(f: A => Boolean): List[A] = {
      flatMap(as)(a => if (f(a)) Cons(a, Nil) else Nil)
    }

    // ex 22
    def addLists(as1: List[Int], as2: List[Int]): List[Int] = {
      @tailrec
      def loop(l1: List[Int], l2: List[Int], result: List[Int]): List[Int] = (l1, l2) match {
        case (Nil, Nil)                   ⇒ result
        case (Cons(h1, t1), Nil)          ⇒ loop(t1, Nil, Cons(h1, result))
        case (Nil, Cons(h2, t2))          ⇒ loop(Nil, t2, Cons(h2, result))
        case (Cons(h1, t1), Cons(h2, t2)) ⇒ loop(t1, t2, Cons(h1 + h2, result))
      }
      reverse(loop(as1, as2, Nil))
    }

    // ex 23
    // if lists unequal len, drops extra from longer list
    def zipWith[A1, A2, B](as1: List[A1], as2: List[A2])(combine: (A1, A2) => B): List[B] = {
      @tailrec
      def loop(l1: List[A1], l2: List[A2], result: List[B]): List[B] = (l1, l2) match {
        case (_, Nil)                     ⇒ result
        case (Nil, _)                     ⇒ result
        case (Cons(h1, t1), Cons(h2, t2)) ⇒ loop(t1, t2, Cons(combine(h1, h2), result))
      }
      reverse(loop(as1, as2, Nil: List[B]))
    }

    // ex 24
    def hasSubsequence[A](sup: List[A], sub: List[A]): Boolean = {
      // traverse 'sup'
      // if we find the first value from 'sub'
      //    if sub is done, return true
      //    if sup is done but sub is not, return false
      //    if neither is done traverse sup.tail with sub.tail
      // we also need to pick up where we left off if subsearch fails
      // but there is still main list left to search
      // do that by traversing the main list with subsearch func

      @tailrec
      def subSearch(main: List[A], seq: List[A]): Boolean = (main, seq) match {
        case (_, Nil)                             ⇒ true
        case (Nil, _)                             ⇒ false
        case (Cons(a, at), Cons(b, bt)) if a == b ⇒ subSearch(at, bt)
        case _                                    ⇒ false
      }

      @tailrec
      def mainSearch(remaining: List[A]): Boolean = remaining match {
        case Nil                                     ⇒ false
        case ls@Cons(_, tail) if !subSearch(ls, sub) ⇒ mainSearch(tail)
        case _                                       ⇒ true
      }

      mainSearch(sup)
    }


    object Tree {
      // ex 25
      def size[A](tree: Tree[A]): Int = tree match {
        case Leaf(_)             ⇒ 1
        case Branch(left, right) ⇒ 1 + size(left) + size(right)
      }

      // ex 26
      def maximum(tree: Tree[Int]): Int = tree match {
        case Leaf(x)             ⇒ x
        case Branch(left, right) ⇒ math.max(maximum(left), maximum(right))
      }

      // ex 27
      def depth[A](tree: Tree[A]): Int = tree match {
        case Leaf(_)             ⇒ 1
        case Branch(left, right) ⇒ 1 + math.max(depth(left), depth(right))
      }

      // ex 28
      def map[A, B](tree: Tree[A], f: A ⇒ B): Tree[B] = tree match {
        case Leaf(x)             ⇒ Leaf(f(x))
        case Branch(left, right) ⇒ Branch(map(left, f), map(right, f))
      }
    }

    object TreeWithFold {
      def fold[A, B](tree: Tree[A])(fl: A ⇒ B)(fb: (B, B) => B): B = tree match {
        case Leaf(x)             ⇒ fl(x)
        case Branch(left, right) ⇒ fb(fold(left)(fl)(fb), fold(right)(fl)(fb))
      }

      def size[A](tree: Tree[A]): Int =
        fold[A,Int](tree)( _ ⇒ 1)((a,b) ⇒ 1 + a + b)

      def maximum(tree: Tree[Int]): Int =
        fold(tree)(x ⇒ x)(math.max)

      def depth[A](tree: Tree[A]): Int =
        fold(tree)(x ⇒ 1)((a, b) ⇒ 1 + math.max(a, b))

      def map[A, B](tree: Tree[A], f: A ⇒ B): Tree[B] =
        fold(tree)(x ⇒ Leaf(f(x)):Tree[B])((a, b) ⇒ Branch(a, b))
    }
  }


  def exercise1: Int = {
    import List._
    val x = List(1, 2, 3, 4, 5) match {
      case Cons(x, Cons(2, Cons(4, _)))          => x
      case Nil                                   => 42
      case Cons(x, Cons(y, Cons(3, Cons(4, _)))) => x + y
      case Cons(h, t)                            => h + sum(t)
      case _                                     => 101
    }
    x
  }

}
