package fpinscala.chapter5

import fpinscala.chapter5.Stream._

/**
 * Created by jefflongueil on 11/15/15.
 */
sealed trait Stream[+A] {
  def headOption: Option[A]

  def headOption2: Option[A]

  def isEmpty: Boolean

  def nonEmpty: Boolean

  // map, filter, append, and flatMap
  def map[B](f: A ⇒ B): Stream[B]

  def filter(f: A ⇒ Boolean): Stream[A]

  def append [B >: A] (st: ⇒ Stream[B]): Stream[B]

  def flatMap[B](f: A ⇒ Stream[B]): Stream[B]

  def toList: List[A] = this match {
    case Empty      ⇒ Nil: List[A]
    case Cons(h, t) ⇒ h() :: t().toList
  }

  def take(n: Int): Stream[A] = this match {
    case Empty | _ if n <= 0 ⇒ Empty
    case Cons(h, t) if n > 0 ⇒ cons[A](h(), t().take(n - 1) )
  }

  def drop(n: Int): Stream[A] = this match {
    case Empty               ⇒ Empty
    case Cons(h, t) if n > 0 ⇒ t().drop(n - 1)
    case s                   ⇒ s
  }

  def foldRight[B](z: => B)(f: (A, => B) => B): B = this match {
    case Cons(h, t) => f(h(), t().foldRight(z)(f))
    case _          => z
  }

  def takeWhile(p: A => Boolean): Stream[A] = this match {
    case Cons(h, t) if p(h()) ⇒ cons(h(), t().takeWhile(p))
    case _                    ⇒ Empty
  }

  def takeWhileViaFoldRight(p: A => Boolean): Stream[A] =
    foldRight(empty[A])((a, b) ⇒ if (p(a)) cons(a, b) else empty)

  def forAll(p: A => Boolean): Boolean = this match {
    case Empty                     ⇒ false
    case Cons(h, t) if t().isEmpty ⇒ p(h())
    case Cons(h, t)                ⇒ if (!p(h())) false else t().forAll(p)
  }
}

case object Empty extends Stream[Nothing] {
  override def headOption: Option[Nothing] = None
  override def isEmpty: Boolean = true
  override def nonEmpty: Boolean = false
  override def headOption2: Option[Nothing] = None
  override def map[B](f: (Nothing) ⇒ B): Stream[B] = empty
  override def flatMap[B](f: (Nothing) ⇒ Stream[B]): Stream[B] = empty
  override def filter(f: (Nothing) ⇒ Boolean): Stream[Nothing] = empty
  override def append[B >: Nothing](st: ⇒ Stream[B]): Stream[B] = empty
}

case class Cons[+A](h: () ⇒ A, t: () ⇒ Stream[A]) extends Stream[A] {
  override def headOption: Option[A] = Some(h())
  override def isEmpty: Boolean = false
  override def nonEmpty: Boolean = true
  override def headOption2: Option[A] =
    foldRight(None: Option[A])((a, _) ⇒ Some(a))

  override def map[B](f: A ⇒ B): Stream[B] =
    Cons(() ⇒ f(h()), () ⇒ t().map(f))
  override def flatMap[B](f: (A) ⇒ Stream[B]): Stream[B] = ???
  override def filter(f: (A) ⇒ Boolean): Stream[A] = ???
  override def append[B >: A](st: ⇒ Stream[B]): Stream[B] = ???
}

object Stream {
  def cons[A](hd: ⇒ A, tl: ⇒ Stream[A]): Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() ⇒ head, () ⇒ tail)
  }

  def empty[A]: Stream[A] = Empty

  def apply[A](as: A*): Stream[A] =
    if (as.isEmpty) empty else cons(as.head, apply(as.tail: _*))
}
